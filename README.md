# Kotlin - Migrating from Python 

This repository contains instructions and code to help developer migrating from Python to get familiar with Kotlin 

## Introduction 
Kotlin is a compiled, statically typed language, which might provide some initial hurdles for people who are used to the interpreted, dynamically typed Python. This document based completly on [this](https://kotlinlang.org/docs/tutorials/kotlin-for-py/introduction.html) aims to explain a substantial portion of Kotlin's syntax and concepts in terms of how they compare to corresponding concepts in Python.

Kotlin can be compiled for several different platforms. In this document, we assume that the target platform is the Java virtual machine, which grants some extra capabilities - in particular, your code will be compiled to Java bytecode and will therefore be interoperable with the large ecosystem of Java libraries.

## Pre-requisite 
Comming from a Python background I assume, you have been using VS code and in this document we will use it as an Editor to get started with understanding how to run Kotlin code. 
- [Download Visual Studio Code](https://code.visualstudio.com/download) 
- [Download Java Developer Kit](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

## How to run the code
To run a particular ```.kt``` file you need to run basiccally $2$ linux command : 
- You first need to compile the `.kt` using `kotlinc fileName.kt -include-runtime -d jars/programName.jar`
- You next run the program `java -jar jars/programName.jar `


_This material was written by [Aasmund Eldhuset](https://eldhuset.net/); it is owned by [Khan Academy](https://www.khanacademy.org/) and is licensed for use under [CC BY-NC-SA 3.0 US](https://creativecommons.org/licenses/by-nc-sa/3.0/us/). Please note that this is not a part of Khan Academy's official product offering._